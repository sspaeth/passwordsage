# -*- coding: utf-8 -*-

# Password Sage. A mobile friendly password manager

# Copyright (C) 2020 Sebastian Spaeth & contributors
# SPDX-License-Identifier: GPL-3.0+
# Author: Sebastian Spaeth <Sebastian@SSpaeth.de>

# allow to return type hints for classes as they are defined
from __future__ import annotations

import gi
gi.require_version('Handy', '1')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gtk, Handy
import logging
from typing import Optional, NoReturn


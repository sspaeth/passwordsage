# -*- coding: utf-8 -*-

# Password Sage. A mobile friendly password manager

# Copyright (C) 2020 Sebastian Spaeth & contributors
# SPDX-License-Identifier: GPL-3.0+
# Author: Sebastian Spaeth <Sebastian@SSpaeth.de>

# allow to return type hints for classes as they are defined
from __future__ import annotations

import gi
gi.require_version('Handy', '1')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gtk, Handy
import logging
import os
from pathlib import Path
import pykeepass
from pykeepass.exceptions import CredentialsError
import traceback
from typing import Optional, NoReturn, Union

class SafeFileChooser(Gtk.FileChooserDialog):
    """File dialog to create or open a safe"""
    pass

class PasswordDialog:
    dialog = None
    
    def __init__(self, app:Gtk.Application) -> None:
        self.app = app
        self.dialog = app.builder.get_object("password_dialog")

    def run(self) -> Optional[str]:
        if self.dialog is None:
            logging.error("Trying to run uninitialized PasswordDialog.")
            return None

        passwd_entry = self.app.builder.get_object("passwd_entry")
        passwd_entry.set_text("")
        response = self.dialog.run()
        self.dialog.hide_on_delete()
        #Note that you probably want to set “input-purpose” to GTK_INPUT_PURPOSE_PASSWORD or GTK_INPUT_PURPOSE_PIN to inform input methods about the purpose of this entry, in addition to setting visibility to FALSE.set_input_purpose () GTK_INPUT_PURPOSE_FREE_FORM

        if response == Gtk.ResponseType.CANCEL:
            return None
        elif response == Gtk.ResponseType.OK:
            return passwd_entry.get_text()
        return None

class Safe():
    """Represents a Keepass Safe

    Instance attributes/properties:
    - version: version of the safe
    - file: Path() to the safe
    - dirty: True if safe has been modified and needs saving.
    - _safe: a PyKeePass()"""

    def __init__(self, app:Gtk.Application,
                 path: Union[str, Path],
                 password:Optional[string] = None,
                 create:Boolean = False) -> None:
        """path of Safe file,
        password can be None and will be queried
        create is True, a new file will be created."""
        self.app = app
        self.dirty = False

        if isinstance(path, str):
            path = Path(path) # convert to Path

        if not create and not path.is_file():
            logging.warning("Not trying to open nonexisting file")
            return None

        # We also want some password
        if password is None:
            password = PasswordDialog(app).run()

        try:
            if create:
                self._safe = pykeepass.create_database(path, password)
            else:
                #TODO: This can take a while. Make async?
                self._safe = pykeepass.PyKeePass(filename=path,
                                                 password=password)
        except CredentialsError as e:
            logging.error("Wrong password")
            raise e

        #Display the contents of the file in our UI
        self.show_kdbx_file()
        return None


    @classmethod
    def create(cls, app:Gtk.Application) -> Optional[Safe]:
        """Create a new Safe() instance and return it. Return None on error"""
        logging.debug("Creating new Safe.")
        
        dialog = Gtk.FileChooserDialog(parent=app.win,
                                       action=Gtk.FileChooserAction.SAVE)
        dialog.set_current_name("passwords.kdbx")
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
        )
        filter = Gtk.FileFilter()
        filter.add_pattern("*.kdbx")
        filter.set_name("Keepass (kdbx)")
        dialog.add_filter(filter)
        filter = Gtk.FileFilter()
        filter.add_pattern("*.*")
        filter.set_name("*.*")
        dialog.add_filter(filter)
        response = dialog.run()

        # Open a file chooser to select file location & name
        filename = None
        if response == Gtk.ResponseType.OK:
            filename = dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            logging.debug("Cancel clicked during file creation dialog.")
        dialog.destroy()
        
        if filename is None:
            return None # Canceled dialog

        path = Path(filename)
        # If filename already exists, ask if we should overwrite it
        if path.is_file():
            logging.warning("Trying to overwrite existing file. Really?")
            overwrite_dlg = app.builder.get_object("overwrite_dlg")
            response = overwrite_dlg.run()
            overwrite_dlg.hide_on_delete()
            if response == Gtk.ResponseType.YES:
                logging.debug("Deleting file")
                os.remove(path) 
            else:
                return None # Do nothing


        try:
            self = cls(app, filename, create = True)
        except Exception as e:
            logging.warning(traceback.format_exc())
            logging.error(e)
            return None
        return self


    @classmethod
    def open(cls, app:Gtk.Application) -> Optional[Safe]:
        """Open an existin Safe() instance and return it. 

        :returns: Safe or `None` on error"""
        
        # Open a file chooser to select file location & name
        logging.debug("Opening existing Safe.")
        dialog = Gtk.FileChooserDialog(parent=app.win,
                                       action=Gtk.FileChooserAction.OPEN)
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
        )
        filter = Gtk.FileFilter()
        filter.add_pattern("*.kdbx")
        filter.set_name("Keepass (kdbx)")
        dialog.add_filter(filter)
        filter = Gtk.FileFilter()
        filter.add_pattern("*.*")
        filter.set_name("*.*")
        dialog.add_filter(filter)
        response = dialog.run()

        filename = None
        if response == Gtk.ResponseType.OK:
            filename = dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            logging.debug("Cancel clicked during file open dialog.")
        dialog.destroy()
        
        if filename is None:
            return None # Canceled dialog, doing nothing

        try:
            self = cls(app, filename, create = False)
        except Exception as e:
            logging.warning(traceback.format_exc())
            logging.error(e)
            return None

        return self


    def show_kdbx_file(self):
        """Display the contents of the file in our UI"""
        logging.debug("Showing KDBX entries")
        main = self.app.builder.get_object("main_content")
        entry_box = self.app.builder.get_object("safe_entry_box")
        entry_list = self.app.builder.get_object("safe_entry_list")
        main.set_visible_child(entry_box)

        for entry in self._safe.entries:
            logging.warning(entry)
            label = Gtk.Label()
            label.set_text(f"{entry.group} {entry.title}\n{entry.username}")
            row = Gtk.ListBoxRow()
            row.password = entry.password
            row.add(label)
            entry_list.insert(row, position=-1)
        entry_list.show_all()

        # Make the close button in the main menu visible
        close_button = self.app.builder.get_object("close_safe_button")
        close_button.show()

    def close(self):
        """Save safe, ask if we want to close and clean up everything.

        :returns: True if we really should close the sage, False if
            the user canceled the action and we should be doing
            nothing.
        """
        # TODO: If closing requires a user confirmation dialog, shoow
        # that one now.

        # TODO: If we don't automatically save on every modification,
        # save things now.
        if self.dirty:
            logging.warning("Need to save safe!. XXX IMPLEMENT")
            raise NotImplementedError("Need to save safe!. XXX IMPLEMENT")

        # Delete all entries that we are currently showing
        entry_list = self.app.builder.get_object("safe_entry_list")
        for row in entry_list.get_children():
            row.destroy()
        #and switch back to the file open view
        main = self.app.builder.get_object("main_content")
        fileopen_stack = self.app.builder.get_object("fileopen_stack")
        main.set_visible_child(fileopen_stack)

        return True # DELETE INSTANCE!
    
    @property
    def version(self):
        """database version as tuple. e.g. (3, 1) is a KDBX v3.1 database."""
        return self._safe.version

    @property
    def file(self):
        """database version as tuple. e.g. (3, 1) is a KDBX v3.1 database."""
        return self._safe.file

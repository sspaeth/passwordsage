# -*- coding: utf-8 -*-

# Password Sage. A mobile friendly password manager

# Copyright (C) 2020 Sebastian Spaeth & contributors
# SPDX-License-Identifier: GPL-3.0+
# Author: Sebastian Spaeth <Sebastian@SSpaeth.de>

import passwordsage
from .safe import Safe

import os.path
import sys
import gi
import logging

gi.require_version('Handy', '1')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gdk, Gio, Gtk, Handy
Handy.init()

class Handler:
    #.app contains the Gtk.Application

    def on_open_button_clicked(self, button:Gtk.Button):
        if self.app.safe is None:
            self.app.safe = Safe.open(self.app)
        return True

    def on_create_button_clicked(self, button:Gtk.Button):
        if self.app.safe is None:
            self.app.safe = Safe.create(self.app)
        return True
    
    def on_main_menu_button(self, button:Gtk.Button):
        #Open the mai menu
        menu = self.app.builder.get_object("main_menu")
        menu.popup_at_widget(button, Gdk.Gravity.SOUTH, Gdk.Gravity.NORTH_EAST,
                             None)
        return False

    def on_about_button(self, button:Gtk.Button):
        """Open the preferences dialog, set icon and version manually"""

        # Version is centrally defined, and the icon is not available
        # from within glade.
        dialog = self.app.builder.get_object("about_dialog")
        dialog.set_logo(self.app.builder.get_object("icon").get_pixbuf())
        dialog.set_version(passwordsage.__VER__)
        dialog.present()
        return True

    def on_settings_button(self, button:Gtk.Button):
        #Open the preferences dialog
        settings = self.app.builder.get_object("settings_win")
        settings.set_transient_for(self.app.win)
        settings.present()
        return False

    def about_dialog_destroy(self, dialog:Gtk.Dialog, data):
        #Hide the about dialog rather than destroy it. Might need it still
        logging.debug("Not really destroying about dialog")
        dialog.hide_on_delete()
        return True

    def settings_dialog_destroy(self, dialog:Gtk.Dialog, event):
        #Hide the settings dialog
        dialog.hide_on_delete()
        return True

    def on_safe_entry_list_row_activated(self, listbox:Gtk.ListBox,
                                         row:Gtk.ListBoxRow):
        """Called when we click on a safe_entry_row.

        Copy password to clipboard."""
        # TODO: Open Detail screen on long click.
        #       Or copy user on left-swipe, password on right-swipe
        # XXX: Is the default Display always the correct one?
        clip = Gtk.Clipboard.get_default(Gdk.Display.get_default ())
        clip.set_text(row.password, -1)
        return True

    def close_safe_button(self, button:Gtk.Button):
        # Close safe and hide close button
        logging.debug("Close safe?!")
        if self.app.safe is not None:
            # close() asks if we really want to do it, or save modifications
            do_it = self.app.safe.close()
            if do_it:
                self.app.safe = None
                button.hide()

        return True

class Application(Gtk.Application):

    def __init__(self):
        super().__init__(application_id='de.sspaeth.passwordsage')
        GLib.set_application_name(_('Password Sage'))
        GLib.set_prgname('de.sspaeth.passwordsage')
        # set to a Safe() instance when opened
        self.safe = None
        
    def do_startup(self):
        # Make resources available...
        self.builder = Gtk.Builder.new_from_resource(
            "/de/sspaeth/PasswordSage/ui/main.ui")
        self.builder.add_from_resource(
            "/de/sspaeth/PasswordSage/ui/preferences.ui")
        cssProvider = Gtk.CssProvider()
        cssProvider.load_from_resource("/de/sspaeth/PasswordSage/ui/style.css")
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            cssProvider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        return Gtk.Application.do_startup(self)
    
    def do_activate(self):
        self.win = self.builder.get_object("mainwin")
        self.win.set_application(self)
        # set app icon
        # 'icon' is defined in main.ui as being resouce
        # de.sspaeth.passwordsage.svg
        icons = [self.builder.get_object("icon").get_pixbuf(),]
        self.win.set_icon_list(icons)

        handler = Handler()
        # make app available from within handler (use weak ref?)
        handler.app = self
        self.builder.connect_signals(handler)
        self.win.show()

    #def do_shutdown(self):
    #    Gtk.Application.do_shutdown(self)
    # Do cleanup work if necessary here...

def main(version):
    # monkeypatch passwordsage version number
    passwordsage.__VER__ = version
    app = Application()
    return app.run(sys.argv)
